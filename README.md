## __Demo Project:__
 Run Nexus on Droplet and Publish Artifact to Nexus  
## __Technologies used:__
 Nexus, DigitalOcean, Linux, Java, Gradle, Maven  
## __Project Description:__  
* Install and configure Nexus from scratch on a cloud server  
* Create new User on Nexus with relevant permissions  
* Java Gradle Project: Build Jar & Upload to Nexus  
* Java Maven Project: Build Jar & Upload to Nexus  
---
## __Detailed Description__
### __Install and configure Nexus from scratch on a cloud server:__
__Cloud server req:__ min 4GB RAM & 2 CPU's  
__Step 1:__ Open SSH port 22  

__Step 2:__ Install java-8

__Step 3:__ Download and install Nexus(into /opt folder) from [__here__](https://help.sonatype.com/repomanager3/product-information/download)  

__Step 4:__ Create Nexus user in your system for Nexus  

__Step 5:__ Change permission for nexus and sonatype-work folders:
```sh
chown -R "nexus-user":"nexus-user" nexus-3.* 
chown -R "nexus-user":"nexus-user" sonatype-work 
```
__Step 6:__ Set Nexus config `vim nexus-3.*/bin/nexus.rc`
```sh
run_as_user=your_user_name_here
```   
__Step 7:__ Switch user to "nexus-user" 

__Step 8:__ Start Nexus:
```sh
/opt/nexus-3.*/bin/nexus start
```
__Don't forget open port 8081 for nexus in your droplet!__

### __Create Nexus user with relevant permissions:__
__Step 1:__ Create Nexus user (e.g. nexus-deployer)  

__Step 2:__ Create role for Maven:
* assign this role: `nx-repository-view-maven2-maven-stapshots-* `

__Step 3:__ Assign role to created user	

### __Java Gradle Project: Build Jar & Upload to Nexus__
__Step 1:__ Open your Gradle project and add plugin for publishing into build.gradle (12 line) 
```java
apply plugin: 'maven-publish'
```
__Step 2:__ Create gradle.properties file for your credentials and put them here 
```java
repoUser = your-user-here
repoPassword = your-pwd-here 
```   
__Step 3:__ Add Publishing block and repositories (__Configure it!__)  :
```java
publishing {
    publications{
        maven(MavenPublication) {
            artifact("build/libs/java-app-$version"+".jar") {
                extension 'jar'
            }
        }
    }
    repositories {
        maven {
            name 'nexus'
            url "http://[your nexus ip]:[your nexus port]/repositories/maven-snapshots/"
            allowInsecureProtocol = true
            credentials {
              username project.repoUser
              password project.repoPassword
            }
        }
    }
}
```
>- `url "http://[your nexus ip]:[your nexus port]/repositories/maven-snapshots/"` - take it from nexus
>- `allowInsecureProtocol = true` - Since you have http port

__Step 5:__ Build app:
```sh
./gradlew build
```

__Step 6:__  Publush app to nexus:
```sh
./gradlew publish
```
### __Java Maven Project: Build Jar & Upload to Nexus__
__Step 1:__ Open Maven project and open file "pom.xml"  

__Step 2:__ Add plugin for publishing and configure it:
* Add it into __\<build>__ Block:
```xml
<pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.8.2</version>
            </plugin>
        </plugins>
 </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
            </plugin>
        </plugins>
```
* Add it outside of __\<build>__ block:
```xml
<distributionManagement>
 <snapshotRepository>
    <id>nexus-snapshots</id>
    <url>http://[your nexus IP]:[nexus port]/repository/maven-snapshots</url>
 </snapshotRepository>
</distributionManagement>
```
>- `<url>http://[your nexus IP]:[nexus port]/repository/maven-snapshots</url>` - paste it from nexus

__Step 3:__ Configure your credentials for Maven:
```sh
cd ~
cd .m2
vim settings.xml 
```
__settings.xml__  holds Your global maven credentials

__Step 4:__ __configure__ your credentials:

```xml
<settings>
    <servers>
        <server>
           <id>nexus-snapshots</id>                     
            <username>your-username-here</username>
            <password>your-password-here</password>
         </server>
     </servers>
</settings>
```
__Step 5:__ Build artifact(from app directory):
```sh
mvn package
```
__Step 6:__ Upload the jar 
```sh
mvn deploy
```
